import Axios from 'axios';

class Request {
	static getInstance() {
		Axios.defaults.withCredentials = true;
		return Request._instance = Request._instance || new Request(); 
	}

	send (url, data = {}, timeout = 1000) {
		return Axios.post(url, data, {timeout})
	}

}

export default Request.getInstance();