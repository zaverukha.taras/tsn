import Vue from 'vue'
import Vuex from 'vuex'
import Request from '@/store/Request.js';

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		appStatus: null,
	},
	mutations: {
		setAppStatus(state, data) {
			state.appStatus = data;
		},
	},
	actions: {
		async init({dispatch}) {
			const response = await Request.send('https://api.vsebankety.ru/api/v1/user/info');
			if (response.data.success) {
				dispatch('setAppStatus', 'authorized');
			} else {
				dispatch('setAppStatus', 'unauthorized');
			}
		},
		setUserData({dispatch}) {
			dispatch('setAppStatus', 'authorized');
		},
		setAppStatus({commit}, data) {
			commit('setAppStatus', data);
		},
		logout({dispatch}) {
			Request.send('https://api.vsebankety.ru/api/v1/user/logout')
			.then(() => {
				dispatch('setAppStatus', 'unauthorized');
			});
		},
	},
	modules: {
	}
})
