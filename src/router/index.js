import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

	const routes = [
	{
		path: '/',
		redirect: '/landlords',
	},
	{
		path: '/landlords',
		name: 'Landlords',
		component: () => import('../views/landlords')
	},
	{
		path: '/landlords/:id',
		name: 'Landlord',
		component: () => import('../views/landlords/Landlord')
	},
	{
		path: '/users',
		name: 'Users',
		component: () => import('../views/users')
	},
	{
		path: '/users/:id',
		name: 'User',
		component: () => import('../views/users/User')
	},
]

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes
})

export default router
