module.exports = {
	"transpileDependencies": [
		"vuetify"
	],
	css: {
		loaderOptions: {
			stylus: {
				use: [require('nib')()],
				import: [
					'~nib/lib/nib/index.styl',
					__dirname + '/src/styles/variables.styl',
				]
			}
		}
	},
	filenameHashing: false,
	chainWebpack(config) {
		config
			.optimization.splitChunks({
				cacheGroups: {
					default: false,
					all: {
						name: 'all',
						test: /.*/,
						chunks: 'all',
						minChunks: 1,
						reuseExistingChunk: true,
					},
				},
			});
	}
}